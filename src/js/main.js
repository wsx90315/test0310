import React from 'react';
import { render } from 'react-dom';

const element1 = <h1>Hello, world</h1>;

render(element1, document.getElementById('root'));

function tick() {
  const element2 = (
    <div>
      <h2>It is {new Date().toLocaleTimeString()}.</h2>
    </div>
  );
  render(element2,document.getElementById('time'));
}

setInterval(tick, 500);


function Welcome(props) {
  return <h1>Hello, {props.name}</h1>;
}

const element3 = <Welcome name="Sara" />;
render(
  element3,
  document.getElementById('root1')
);